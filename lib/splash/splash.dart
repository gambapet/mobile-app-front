import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: double.infinity,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/background-image.png"),
                  fit: BoxFit.cover),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 48),
            child: Stack(
              children: [
                Container(
                    width: 2000,
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: Image.asset('assets/gambapet.png',
                              width: 100, height: 100),
                        )
                      ],
                    )),
                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 150),
                  child: const Text(
                    "GambaPet",
                    style: TextStyle(
                      color: Color(0xFFFFFFFF),
                      fontSize: 40,
                      fontFamily: 'Coco Bold',
                    ),
                  ),
                ),
              ],
            )));
  }
}
